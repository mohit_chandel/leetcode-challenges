//https://leetcode.com/explore/challenge/card/june-leetcoding-challenge/539/week-1-june-1st-june-7th/3347/
/////////Recursive post-order DFS///////////////

var invertTree = function(root) {
    let postOrder = function(node) {
        if(node) {
            postOrder(node.left);
            postOrder(node.right);
            let temp = node.left;
            node.left = node.right;
            node.right = temp;
        }
    }
    postOrder(root);
    return root;
};