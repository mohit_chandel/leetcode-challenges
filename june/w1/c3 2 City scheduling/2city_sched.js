/**
 * @param {number[][]} costs
 * @return {number}
 */
var twoCitySchedCost = function(costs) {
    let heap = [];
    let insert = function(element) {
        heap.push(element);
        for(let i = heap.length - 1 ; i > 0 ;  ) {
            let parent = Math.floor((i - 1)/2);
            if(heap[parent].val < element.val) {
                heap[i] = {...heap[parent]};
                heap[parent] = element;
                i = parent;
            } else break;
        }
    }
    
    let remove = function() {
        let removed = {...heap[0]};
        heap[0] = heap.pop();
        if(heap.length){
            for(let i = 0; i * 2 + 2 < heap.length; ){
                let child = heap[i*2 + 1].val > heap[i*2 + 2].val ? i*2+1 : i*2+2;
                if(heap[i].val < heap[child].val) {
                    let temp = {...heap[i]}
                    heap[i] = {...heap[child]};
                    heap[child] = temp;
                    i = child;
                } else break;
            }
        }
        return removed;
    }
        
    for(let i = 0; i < costs.length; i++ ){
        insert({
            val: costs[i][1] - costs[i][0],
            index: i
        });
    }
    let sum = 0;
    for(let i = 0; i < costs.length; i++) {
        let node = remove();
        if(i < costs.length/2) {
            sum = sum + costs[node.index][0] 
        } else {
            sum = sum + costs[node.index][1]
        }
    }
    return sum;
};