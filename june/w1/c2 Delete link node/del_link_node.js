//https://leetcode.com/explore/challenge/card/june-leetcoding-challenge/539/week-1-june-1st-june-7th/3348/
////////////Iterate////////////
var deleteNode = function(node) {
    while(node.next.next) {
        node.val = node.next.val;
        node = node?.next;
    }
    node.val = node.next.val;
    node.next = null;
};
		 
/////Copy next node and delete it//////////////
var deleteNode = function(node) {
    node.val = node.next.val;
    node.next = node.next.next;
};