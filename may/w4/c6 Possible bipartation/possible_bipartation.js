//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/537/week-4-may-22nd-may-28th/3342/
/**
 * @param {number} N
 * @param {number[][]} dislikes
 * @return {boolean}
 */
var possibleBipartition = function(N, dislikes) {
    if(dislikes.length == 0) return true;
    
    let graph = {};
    let queue = [];
    let colors = new Array(N).fill(-1);

    for(let i = 0; i < dislikes.length ; i++){
        if(graph[dislikes[i][0]]){graph[dislikes[i][0]].push(dislikes[i][1]);}
        else graph[dislikes[i][0]] = [dislikes[i][1]];
        if(graph[dislikes[i][1]]){graph[dislikes[i][1]].push(dislikes[i][0]);}
        else graph[dislikes[i][1]] = [dislikes[i][0]];
    }

    let checkBiPart = function(element){
        queue.push(element);
        if(colors[element-1] == -1)colors[element - 1] = 1
        queue.push(element);
        while(queue.length > 0){
            let el = parseInt(queue.shift()) 
            for(let i of graph[el]){
                if(colors[i-1] == -1){
                    colors[i-1] = 1 - colors[el-1];
                    queue.push(i);
                }else if(colors[i-1] != 1 - colors[el - 1]) return false;
            }
        }
    }
    
    while(true){
        let unvisited = colors.findIndex(value => value == -1);
        if( unvisited == -1) return true;
        if( graph[unvisited+1] === undefined ) {colors[unvisited]=2; continue};
        if(checkBiPart(unvisited+1) == false ) return false;

    }
};