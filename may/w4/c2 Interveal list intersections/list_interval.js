//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/537/week-4-may-22nd-may-28th/3338/
/**
 * @param {number[][]} A
 * @param {number[][]} B
 * @return {number[][]}
 */
var intervalIntersection = function(A, B) {
    let res = []
    
    for(let i = 0, j = 0; i < A.length && j < B.length ;){
        if(A[i][1] >= B[j][0] && A[i][0] <= B[j][1]) {
                res.push([Math.max(A[i][0],B[j][0]),Math.min(A[i][1],B[j][1])])
        }
        A[i][1] < B[j][1] ? i++ : j++;
        
    }
    return res;
};