//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/537/week-4-may-22nd-may-28th/3340/
////////////Using recursion ////////////////////
/**
 * @param {number[]} A
 * @param {number[]} B
 * @return {number}
 */
var maxUncrossedLines = function(A, B) {
    let LCS = function (i,j){
        if(i >= A.length || j >= B.length) return 0;
        else if(A[i] == B[j]) return 1 + LCS(i+1, j+1);
        else return Math.max(LCS(i+1, j) , LCS(i, j+1));
    }
    return LCS(0,0);
};

///////////Using Dynamic Programing////////////////
/**
 * @param {number[]} A
 * @param {number[]} B
 * @return {number}
 */
 
var maxUncrossedLines = function(A, B) {
    let result = new Array(A.length + 1);
    result[0] = new Array(B.length + 1).fill(0);
    for(let i = 1 ; i <= A.length ; i++ ){
        result[i] = new Array(B.length + 1).fill(0);
        for(let j = 1; j <= B.length ; j++){
            if(A[i-1] == B[j-1]){
                result[i][j] = 1 + result[i-1][j-1]; 
            }
            else{
                result[i][j] = Math.max(result[i-1][j] , result[i][j-1]);
            }
        }
    }
    return result[A.length][B.length]
    
};