//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/537/week-4-may-22nd-may-28th/3337/
/**
 * @param {string} s
 * @return {string}
 */
var frequencySort = function(s) {
    let a = {}
    for(let c of s){
        a[c] = a[c]+1 || 1;
    }
    
    return Object.keys(a).sort((k1,k2)=>a[k2]-a[k1]).reduce((acc,item)=>{
        return acc + item.repeat(a[item]); 
    },"");

};