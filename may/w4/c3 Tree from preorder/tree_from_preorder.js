//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/537/week-4-may-22nd-may-28th/3339/
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} preorder
 * @return {TreeNode}
 */
var bstFromPreorder = function(preorder) {   
    let pos = 1;
    let insertNode = function(min, max, curr){
        if( pos == preorder.length || preorder[pos] < min || preorder[pos] > max ){ return ; }
        if(preorder[pos]<curr.val){
            curr.left = new TreeNode(preorder[pos],null,null);
            pos++;
            insertNode(min,curr.val-1,curr.left);
        }
        if( pos == preorder.length || preorder[pos] < min || preorder[pos] > max ){ return ; }
        curr.right = new TreeNode(preorder[pos],null,null);
        pos++;
        insertNode(curr.val+1,max,curr.right);
        
    }
    
    let root = new TreeNode(preorder[0],null,null)
    insertNode(Number.MIN_SAFE_INTEGER , Number.MAX_SAFE_INTEGER, root)
    
    return root;
};