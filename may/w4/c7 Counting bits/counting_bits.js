/**
 * @param {number} num
 * @return {number[]}
 */
var countBits = function(num) {
    let a = new Array(num.length).fill(0);
    
    for(let i = 1; i <= num; i++){
        if(i % 2 == 0){
            a[i] = a[Math.floor(i/2)]
        }else{
            a[i] = a[Math.floor(i/2)] + 1
        }
    }
    return a;
};