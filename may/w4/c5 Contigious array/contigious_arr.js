//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/537/week-4-may-22nd-may-28th/3341/
/////////else if not working//////////

/**
 * @param {number[]} nums
 * @return {number}
 */
var findMaxLength = function(nums) {
    let map = {};
    let largestSubarr = 0 , count = 0;
    
    
    for(let i = 0; i < nums.length; i++){
        nums[i] == 0 ? count-- : count++;
        if(count == 0) {
            largestSubarr = i+1;
        }
        else if(!!map[count]){
            largestSubarr = largestSubarr < i - map[count] ? i - map[count] : largestSubarr;                                 console.log("here")
        }
        else map[count] = i ;
        console.log(count , " ",!map[count]);
//      console.log("count: ",count, "| ","map: ", map, "| ","largestSubArr: ", largestSubarr, "i: ", i)
    }
    return largestSubarr;
};

///////////////////using maps//////////////////
/**
 * @param {number[]} nums
 * @return {number}
 */
var findMaxLength = function(nums) {
    let map = {};
    let largestSubarr = 0 , count = 0;
    
    
    for(let i = 0; i < nums.length; i++){
        nums[i] == 0 ? count-- : count++;
        if(count == 0) {
            largestSubarr = i+1;
        }
        else if(map[count] === undefined){
            map[count] = i;
        }
        else
            largestSubarr = Math.max(i - map[count] , largestSubarr);
        
    }
    return largestSubarr;
};