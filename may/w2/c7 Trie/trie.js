//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/535/week-2-may-8th-may-14th/3329/
/**
 * Initialize your data structure here.
 */
var Trie = function() {
    
};

/**
 * Inserts a word into the trie. 
 * @param {string} word
 * @return {void}
 */


function TrieNode(key){
    this.key=key;
    this.children={};
    this.end=false;
}

var Trie = function(){
    this.root= new TrieNode(null);
}


Trie.prototype.insert = function(word) {
    let node = this.root;
    for(let i=0;i<word.length;i++){
        if(!node.children[word[i]]){
            node.children[word[i]]=new TrieNode(word[i]);
        }
        node=node.children[word[i]]
        if(i==word.length-1)
            node.end=true;
    }
};

/**
 * Returns if the word is in the trie. 
 * @param {string} word
 * @return {boolean}
 */
Trie.prototype.search = function(word) {
    let node = this.root;
    for(let i of word){
        if(node.children[i]){
            node = node.children[i];
        }else return false;
    }
    return node.end;
};

/**
 * Returns if there is any word in the trie that starts with the given prefix. 
 * @param {string} prefix
 * @return {boolean}
 */
Trie.prototype.startsWith = function(prefix) {
    let node = this.root;
    let output = []
    for(let i of prefix){
        if(node.children[i]){
            node = node.children[i]
        }else{
            return false;
        }
    }
    return true;
};


/** 
 * Your Trie object will be instantiated and called as such:
 * var obj = new Trie()
 * obj.insert(word)
 * var param_2 = obj.search(word)
 * var param_3 = obj.startsWith(prefix)
 */