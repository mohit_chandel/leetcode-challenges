//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/535/week-2-may-8th-may-14th/3328/
/**
 * @param {string} num
 * @param {number} k
 * @return {string}
 */
var removeKdigits = function(num, k) {
    //if(num.length<=k)return "0";
    let stack = [];
    let removed=0;
    for(let n of num){
        while(removed<k && stack.length && n<stack[stack.length-1]){
            stack.pop();
            removed++;
        }
        stack.push(n);
    }
    while(removed<k){
        stack.pop();
        removed++;
    }
    while(stack.length && stack[0]==0)
        stack.shift();
    return stack.join("") || "0";

};