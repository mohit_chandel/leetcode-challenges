// https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/535/week-2-may-8th-may-14th/3325/
//////////////////Using directed graph//////////////////////
var findJudge = function(N, trust) {
    const counts = new Array(N+1).fill(0);
    for(let item of trust){
        counts[item[0]]--;
        counts[item[1]]++
    }
    for(let i=1;i<=N;i++){
        if(counts[i]==N-1) return i; 
    }return -1;
};