//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/535/week-2-may-8th-may-14th/3327/

///////////////////Using binary search//////////////

var singleNonDuplicate = function(nums) {
    let lo=0,hi=nums.length-1;
    while(lo<hi){
        let mid = lo + Math.floor((hi-lo)/2);
		
/* 		
	This is condensed to the code below
		if(mid%2==0){
            if(nums[mid]==nums[mid+1])lo=mid+2;
            else hi=mid;
        }else{
            if(nums[mid-1]==nums[mid])lo=mid+1;
            else hi=mid
        } */
		
        let temp = mid%2==0?mid+1:mid-1;
        nums[mid]==nums[temp]?lo=mid+1:hi=mid;
        

        
    }return nums[lo]
};