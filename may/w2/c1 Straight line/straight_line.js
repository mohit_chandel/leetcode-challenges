//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/535/week-2-may-8th-may-14th/3323/
var checkStraightLine = function(coordinates) {
    if(coordinates.length==2)return true;
    let slope = (coordinates[0][1]-coordinates[1][1])/(coordinates[0][0]-coordinates[1][0]);
    let c = coordinates[0][1]-(slope*coordinates[0][0]);
    
    for(let i=2;i<coordinates.length;i++){
        if(!isFinite(slope)){
            if( coordinates[0][0]!=coordinates[i][0])return false;}
        else if((coordinates[i][1]-c)!=slope*coordinates[i][0]){return false};
    }
    return true;
};