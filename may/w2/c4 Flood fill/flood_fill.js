//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/535/week-2-may-8th-may-14th/3326/
////////////////////Recursion////////////
var floodFill = function(image, sr, sc, newColor) {
    if(image[sr][sc]==newColor)return image;
    let colorPixle = function(sr,sc,oldColor){
        if(sr<0 || sc<0 || sr> image.length-1 || sc>image[0].length-1 || image[sr][sc]!=oldColor ) return;
        image[sr][sc]=newColor;
        colorPixle(sr-1,sc,oldColor);
        colorPixle(sr+1,sc,oldColor);
        colorPixle(sr,sc-1,oldColor);
        colorPixle(sr,sc+1,oldColor);
    } 
    colorPixle(sr,sc,image[sr][sc]);
    return image;
};