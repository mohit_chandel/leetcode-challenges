//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/538/week-5-may-29th-may-31st/3344/
//////////////////Cycle detection using Dfs////////////////////////
/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {boolean}
 */
var canFinish = function(numCourses, prerequisites) {
    if(prerequisites.length == 0){return true;}
    
    let adjList = {};

    for(let edge of prerequisites){
        adjList[edge[0]] ? adjList[edge[0]].push(edge[1]) : adjList[edge[0]] = [edge[1]];
    }

    let detectCycle = function(key, visited, recursionStack){
        if(!visited[key]){
            visited[key] = true;
            recursionStack[key] = true;
            let neighbours = adjList[key] || [] ;
            
            for(let neigh of neighbours) {
                if(!visited[neigh] && detectCycle(neigh, visited, recursionStack)){
                    return true;
                } else if(recursionStack[neigh]) {
                    return true;
                }
            }
        }
        recursionStack[key] = false;
        return false;
    }    
    
    for(var key of Object.keys(adjList)){
        let visited = {};
        let recursionStack = {};
        if(detectCycle(key, visited, recursionStack)) {
            return false;
        }
        
    } return true;
    

};

