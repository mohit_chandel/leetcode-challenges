https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/538/week-5-may-29th-may-31st/3346/

/**
 * @param {string} word1
 * @param {string} word2
 * @return {number}
 */
var minDistance = function(word1, word2) {
    if(word1.length == 0) return word2.length;
    let arr = [[],[]];
    for (let i=0 ;i <= word1.length; i++) {
        arr[0].push(i);
    }
    let row = 0;
    for(let m = 1; m <= word2.length ; m++){
        row = m % 2;
        arr[row][0] = m;
        for(let n = 1; n <= word1.length ; n++){
            let refRow = row == 0 ? 1: 0;
            let min = Math.min(arr[refRow][n],arr[refRow][n-1],arr[row][n-1]);
            arr[row][n] = word1[n-1] == word2[m-1] ? arr[refRow][n-1] : min + 1;
        }
    }
    return arr[row][word1.length];
};
