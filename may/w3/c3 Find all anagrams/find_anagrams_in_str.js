https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/536/week-3-may-15th-may-21st/3332/
/**
 * @param {string} s
 * @param {string} p
 * @return {number[]}
 */

var findAnagrams = function(s, p) {
    let a = [];
    let result =[];
    let s_map ={};
    let st_index = 0;

    let p_map= p.split('').reduce((acc,item)=>{
        acc[item]=acc[item]+1||1;
        s_map[item]=0;
        return acc;
    },{});
    
    for(let i=0;i<s.length;i++){
        a.push(s[i]);
        if(p_map[s[i]] === undefined ){
            a=[];
            st_index=i+1;
            for (var key in p_map) {
                s_map[key] = 0;
            }
            continue;
        }
        s_map[s[i]] += 1;
        if(a.length==p.length){
            if(!Object.keys(p_map).some((item)=>{return p_map[item]!=s_map[item]})){
                result.push(st_index);
            }
            let dequed = a.shift()
            if(p_map[dequed]!== undefined) s_map[dequed]-=1;
            st_index++;
        
        }

    }
    return result;
};