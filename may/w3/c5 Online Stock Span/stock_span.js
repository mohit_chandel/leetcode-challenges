// https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/536/week-3-may-15th-may-21st/3334/
var StockSpanner = function() {
     this.prices=[];
};

/** 
 * @param {number} price
 * @return {number}
 */
StockSpanner.prototype.next = function(price) {
    let count = 1;
    

    while(this.prices.length>0 && this.prices[this.prices.length-1][0]<=price){
        count+=this.prices[this.prices.length-1][1];
        this.prices.pop();
    }
    
    this.prices.push([price,count])

    return count;

    
};

/** 
 * Your StockSpanner object will be instantiated and called as such:
 * var obj = new StockSpanner()
 * var param_1 = obj.next(price)
 */