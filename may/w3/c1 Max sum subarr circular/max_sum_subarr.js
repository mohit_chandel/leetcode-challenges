// https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/536/week-3-may-15th-may-21st/3330/

var maxSubarraySumCircular = function(A) {
    let max_sum = A[0];
    
    for(let i=0;i<A.length;i++){
        let count =0;
        let curr_max = A[i];
        for(let j=i+1;count<A.length-1;count++){
            if(j>=A.length)j=0;
            curr_max = Math.max(A[j],A[j]+curr_max);
            max_sum = Math.max(max_sum,curr_max);
            j++;
        }
    }
    return max_sum;
};