// https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/536/week-3-may-15th-may-21st/3335/
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} k
 * @return {number}
 */
 
 
var kthSmallest = function(root, k) {
    let arr 
    var inOrder = function(node){
        console.log("start")
        if(k==0)return
        if(node == null)return;
        inOrder(node.left,arr);
        if(k==0)return;
        arr=node.val;
        k--;
        if(k==0)return
        inOrder(node.right,arr);
        
    }
    
    inOrder(root);
    return arr;
};