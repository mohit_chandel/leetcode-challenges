// https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/536/week-3-may-15th-may-21st/3333/
/**
 * @param {string} s1
 * @param {string} s2
 * @return {boolean}
 */
var checkInclusion = function(s1, s2) {
    let s2_map = {}
    let s1_map = s1.split('').reduce((acc,item)=>{
        acc[item]= acc[item]+1 || 1;
        s2_map[item]= 0;
        return acc;
    },{});
    let a =[];
    
    for(let i in s2){
        a.push(s2[i]);
        if(s1_map[s2[i]] === undefined){
            a=[];
            Object.keys(s2_map).forEach(item=>s2_map[item]=0);
            continue;
        }
        s2_map[s2[i]]+=1;
        if(s1.length == a.length){
            if(!Object.keys(s1_map).some(item=>{return s1_map[item]!=s2_map[item]})){
                return true;
            }
            s2_map[a.shift()]-=1;
        }
        
    }
    return false;
};