// https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/536/week-3-may-15th-may-21st/3336/
/**
 * @param {number[][]} matrix
 * @return {number}
 */
var countSquares = function(matrix) {
    let sum = 0;
    for(let i in matrix){
        for(let j in matrix[i]){
            if(matrix[i][j] == 0) continue;
            if(i == 0 || j == 0){
                if(matrix[i][j] == 1) sum++;
                continue;
            }
            matrix[i][j] =  Math.min(matrix[i-1][j], matrix[i][j-1], matrix[i-1][j-1]) + 1; 
            sum = sum + matrix[i][j];
        }
    }
    return sum;
};