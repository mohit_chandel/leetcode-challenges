//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/534/week-1-may-1st-may-7th/3322/
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} x
 * @param {number} y
 * @return {boolean}
 */
var isCousins = function(root, x, y) {
    function getDepthAndParent(node , n , depth=0, parent){
        if(!node)return null;
        if(node.val===n)return{depth,parent};
        let left = getDepthAndParent(node.left,n,depth+1,node);
        if(left)return left;
        let right = getDepthAndParent(node.right,n,depth+1,node);
         if(right)return right;
    }
    let {depth:xdepth,parent:xparent} = getDepthAndParent(root, x);
    let {depth:ydepth,parent:yparent} = getDepthAndParent(root, y);
    return xdepth===ydepth && xparent !== yparent;
};