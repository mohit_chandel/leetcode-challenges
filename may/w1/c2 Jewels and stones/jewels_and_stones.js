//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/534/week-1-may-1st-may-7th/3317/
var numJewelsInStones = function(J, S) {
    let count = 0;
    let jewels = new Map()
    
    for(let j of J)
        jewels.set(j,true);
    for(let s of S)
        if(jewels.get(s))count ++;
    return count;       
};