//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/534/week-1-may-1st-may-7th/3319/
var canConstruct = function(ransomNote, magazine) {
   let countRansomNote = ransomNote.split('').reduce((accumulator,item)=>{
        if(accumulator[item])accumulator[item]++;
        else accumulator[item]=1;
       return accumulator;
   },{});
    
       let countMagzine= magazine.split('').reduce((accumulator,item)=>{
        if(accumulator[item])accumulator[item]++;
        else accumulator[item]=1;
       return accumulator;
   },{});
    
    for(let [key,value] of Object.entries(countRansomNote))
        {   
            console.log(key)
            if(countMagzine[key]){
            if(countMagzine[key]<countRansomNote[key]) return false;
            }else return false
        }
    
    return true;
    
};