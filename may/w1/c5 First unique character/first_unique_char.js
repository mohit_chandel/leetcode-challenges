//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/534/week-1-may-1st-may-7th/3320/

var firstUniqChar = function(s) {
    let map={}
    for(let i=0;i<s.length;i++){
        let c = s.charAt(i);
        if(map[c])continue;
        //if(s.indexOf(c,i+1)==-1)return i
        if(s.indexOf(c)==s.lastIndexOf(c))return i;
        map[c]=true
    }return -1;
};