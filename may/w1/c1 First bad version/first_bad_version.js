//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/534/week-1-may-1st-may-7th/3316/

var majorityElement = function(nums) {
    let a = {}
    let limit =Math.floor(nums.length/2)

    for(let no of nums){
        a[no] = a[no]+1 || 1;
        //if(a[no]){a[no]+=1}else {a[no]=1}
        if(a[no]>limit)return no;
    }
};