//https://leetcode.com/explore/challenge/card/may-leetcoding-challenge/534/week-1-may-1st-may-7th/3321/
////////Using maps///////////////////
var majorityElement = function(nums) {
    let a = {}
    let limit =Math.floor(nums.length/2)

    for(let no of nums){
        a[no] = a[no]+1 || 1;
        //if(a[no]){a[no]+=1}else {a[no]=1}
        if(a[no]>limit)return no;
    }
    
};

/////Using Random/////////

var majorityElement = function(nums) {
    let majority= nums.length/2;
    while(true){
        let candidate = nums[Math.floor(Math.random()*nums.length)];
        let count =0;
        for(let i of nums){
            count=i==candidate?count+1:count
            if(count>majority)return candidate;
        }
    }
};

///////////////////Using Divide and conquer//////////////////
var majorityElement = function(nums) {

    var countInRange = function(ar,no, lo,hi){
        let count =0;
        for(let i=lo;i<=hi;i++){
            if(ar[i]==no)count++;
        }
        return count;
        
    }
    
   var majorityElementRec=function(arr, lo, hi){
       
        if(lo==hi)return nums[lo];
        
        let mid = Math.floor((hi-lo)/2)+lo;
        console.log(mid)
        let left = majorityElementRec(nums,lo, mid); //1
        let right = majorityElementRec(nums, mid+1,hi); 
        if(left == right)return left;
        
        let leftCount = countInRange(nums, left, lo, hi)
        let rightCount = countInRange(nums, right, lo, hi)
		if(leftCount==rightCount)return left;
        return leftCount>rightCount?left:right;
    }
   return majorityElementRec(nums, 0,nums.length-1);
    
};

/////////////////Count ++ && --///////////////////
var majorityElement = function(nums) {
    let count =0;
    let candidate;
    for(let no of nums){
        if(count==0)candidate=no;
        count+=(no==candidate)?1:-1;
    }
    return candidate;
};